#ifndef __BC28_H
#define __BC28_H	
#include "usart2.h"
#include <stm32f10x.h>
#include "delay.h"
void Clear_Buffer(void);//清空缓存	
int BC28_Init(void);
void BC28_PDPACT(void);;
void BC28_RECData(void);
void BC28_RegALIYUNIOT(void);
void BC28_ALYIOTSenddataAdc1(u8 *len,u8 *data);
void BC28_ALYIOTSenddataAdc(u8 *data1,u8 *data2);
typedef struct
{
   uint8_t CSQ;    
   uint8_t Socketnum;   //编号
   uint8_t reclen[10];   //获取到数据的长度
   uint8_t res;      
   uint8_t recdatalen[10];
   uint8_t recdata[100];
} BC28;

#endif
