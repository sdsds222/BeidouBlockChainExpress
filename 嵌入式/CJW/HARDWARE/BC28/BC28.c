///AT+NSOCL=0
#include "BC28.h"
#include "string.h"
#include "usart2.h"

char *strx,*extstrx;
char atstr[BUFLEN];
int err;    					//全局变量
char atbuf[BUFLEN];
char objtnum[BUFLEN];	//观察号
char distnum[BUFLEN];	//观察号
BC28 BC28_Status;

////////////////////////只要修改三要素/////////////////////////////////
#define DEVICENAME "emqx_MDg2Mj"           //修改设备名称
#define ACCOUNT "admin" //账号
#define PASSWORD "public" //账号



void Clear_Buffer(void)//清空串口2缓存
{
    printf(buf_uart2.buf);  //清空前打印信息
    delay_ms(300);
    buf_uart2.index=0;
    memset(buf_uart2.buf,0,BUFLEN);
}

int BC28_Init(void)
{
    int errcount = 0;
    err = 0;    //判断模块卡是否就绪最重要
    printf("start init BC28\r\n");
		Clear_Buffer();	
    Uart2_SendStr("ATE1\r\n");
    delay_ms(3000);
    printf(buf_uart2.buf);      //打印收到的串口信息
    printf("get back BC28\r\n");
    strx=strstr((const char*)buf_uart2.buf,(const char*)"OK");//返回OK
    while(strx==NULL)
    {
        printf("\r\n单片机正在连接到模块...\r\n");
        Clear_Buffer();	
        Uart2_SendStr("ATE1\r\n");
        delay_ms(300);
			  printf(buf_uart2.buf);
        strx=strstr((const char*)buf_uart2.buf,(const char*)"OK");//返回OK
    }
    
    Uart2_SendStr("AT+CIMI\r\n");//获取卡号，类似是否存在卡的意思，比较重要。
    delay_ms(300);
    strx=strstr((const char*)buf_uart2.buf,(const char*)"ERROR");//只要卡不错误 基本就成功
    if(strx==NULL)
    {
        printf("我的卡号是 : %s \r\n",&buf_uart2.buf[8]);
        Clear_Buffer();	
        delay_ms(300);
    }
    else
    {
        err = 1;
        printf("卡错误 : %s \r\n",buf_uart2.buf);
        Clear_Buffer();
        delay_ms(300);
    }

    Uart2_SendStr("AT+CGSN=1\r\n");//激活网络，PDP
    delay_ms(300);
    strx=strstr((const char*)buf_uart2.buf,(const char*)"OK");//返OK
    Clear_Buffer();	
    if(strx)
    {
        Clear_Buffer();	
        printf("GET IMEI OK\r\n");
        delay_ms(300);
    }
		
    Uart2_SendStr("AT+CGATT?\r\n");//查询激活状态
    delay_ms(300);
    strx=strstr((const char*)buf_uart2.buf,(const char*)"+CGATT:1");//返1 表明激活成功 获取到IP地址了
    Clear_Buffer();	
    errcount = 0;
    while(strx==NULL)
    {
        errcount++;
        Clear_Buffer();	
        Uart2_SendStr("AT+CGATT?\r\n");//获取激活状态
        delay_ms(300);
        strx=strstr((const char*)buf_uart2.buf,(const char*)"+CGATT:1");//返回1,表明注网成功
        if(errcount>100)     //防止死循环
        {
            err=1;
            errcount = 0;
            break;
        }
    }


    Uart2_SendStr("AT+NBAND?\r\n"); //允许错误值
    delay_ms(300);
    strx=strstr((const char*)buf_uart2.buf,(const char*)"OK");//返回OK
    if(strx)
    {
        printf("========BAND========= \r\n %s \r\n",buf_uart2.buf);
        Clear_Buffer();
        delay_ms(300);
    }
		//获取信号质量
    Uart2_SendStr("AT+CSQ\r\n");//查看获取CSQ值
    delay_ms(300);
    strx=strstr((const char*)buf_uart2.buf,(const char*)"+CSQ");//返回CSQ
    if(strx)
    {
        printf("信号质量:%s\r\n",buf_uart2.buf);
        Clear_Buffer();
        delay_ms(300);
    }
		//获取注册状态
    Uart2_SendStr("AT+CEREG?\r\n");
    delay_ms(300);
    strx=strstr((const char*)buf_uart2.buf,(const char*)"+CEREG:0,1");//返回注册状态
    extstrx=strstr((const char*)buf_uart2.buf,(const char*)"+CEREG:1,1");//返回注册状态
    Clear_Buffer();	
    errcount = 0;
    while(strx==NULL&&extstrx==NULL)//两个返回值都没有
    {
        errcount++;
        Clear_Buffer();
        Uart2_SendStr("AT+CEREG?\r\n");//判断运营商
        delay_ms(300);
        strx=strstr((const char*)buf_uart2.buf,(const char*)"+CEREG:0,1");//返回注册状态
        extstrx=strstr((const char*)buf_uart2.buf,(const char*)"+CEREG:1,1");//返回注册状态
        if(errcount>100)     //防止死循环
        {
            err=1;
            errcount = 0;
            break;
        }
    }
    return err;
}

//void BC28_PDPACT(void)//激活场景，为连接服务器做准备
//{
//    int errcount = 0;
//    Uart2_SendStr("AT+CGPADDR\r\n");//激活场景
//    delay_ms(300);
//    Clear_Buffer();
//    Uart2_SendStr("ATI\r\n");//获取版本
//    delay_ms(300);
//    Clear_Buffer();
//    Uart2_SendStr("AT+CGATT?\r\n");//激活场景
//    delay_ms(300);
//    strx=strstr((const char*)buf_uart2.buf,(const char*)"+CGATT:1");//注册上网络信息
//    Clear_Buffer();	
//    while(strx==NULL)
//    {
//        errcount++;
//        Clear_Buffer();
//        Uart2_SendStr("AT+CGATT?\r\n");//激活场景
//        delay_ms(300);
//        strx=strstr((const char*)buf_uart2.buf,(const char*)"+CGATT:1");//一定要终端入网
//        if(errcount>100)     //防止死循环
//        {
//            errcount = 0;
//            break;
//        }
//    }
//    Clear_Buffer();
//}


void BC28_RegALIYUNIOT(void)//平台注册
{
    Uart2_SendStr("AT+QMTCLOSE=0\r\n");//Disconnect a client from MQTT server
    delay_ms(300);
    Clear_Buffer();

    Uart2_SendStr("AT+QMTDISC=0\r\n");//删除句柄
    delay_ms(300);
    Clear_Buffer();

//    memset(atstr,0,BUFLEN);
//    sprintf(atstr,"AT+QMTCFG=\"ALIAUTH\",0,\"%s\",\"%s\",\"%s\"\r\n",PRODUCEKEY,DEVICENAME,DEVICESECRET);
//    Uart2_SendStr(atstr);			//发送阿里云配置参数
//    delay_ms(300);  					//等待300ms反馈OK
//    strx=strstr((const char*)buf_uart2.buf,(const char*)"OK");//返OK
//    while(strx==NULL)
//    {
//        strx=strstr((const char*)buf_uart2.buf,(const char*)"OK");//返OK
//    }
//    Clear_Buffer();

    Uart2_SendStr("AT+QMTOPEN=0,\"116.205.228.85\",1883\r\n");//登录阿里云平台 需要等待5分钟
    delay_ms(300);
    strx=strstr((const char*)buf_uart2.buf,(const char*)"+QMTOPEN: 0,0");//返+QMTOPEN: 0,0
    while(strx==NULL)
    {
        strx=strstr((const char*)buf_uart2.buf,(const char*)"+QMTOPEN: 0,0");//返+QMTOPEN: 0,0
    }
    Clear_Buffer();

		//链接设备，连接到设备
    memset(atstr,0,BUFLEN);
    sprintf(atstr,"AT+QMTCONN=0,\"emqx_MDg2Mj\",\"admin\",\"public\"\r\n");
    printf("atstr = %s \r\n",atstr);
    Uart2_SendStr(atstr);		//发送链接到阿里云
    delay_ms(300);
    strx=strstr((const char*)buf_uart2.buf,(const char*)"+QMTCONN: 0,0,0");//返+QMTCONN: 0,0,0
    while(strx==NULL)
    {
        strx=strstr((const char*)buf_uart2.buf,(const char*)"+QMTCONN: 0,0,0");//返+QMTCONN: 0,0,0
    }
    Clear_Buffer();

    memset(atstr,0,BUFLEN);
    sprintf(atstr,"AT+QMTSUB=0,1,\"testtopic\",0\r\n");
    printf("atstr = %s\r\n",atstr);
    Uart2_SendStr(atstr);//订阅
    delay_ms(300);
    strx=strstr((const char*)buf_uart2.buf,(const char*)"+QMTSUB: 0,1,0,0");//返OK
    while(strx==NULL)
    {
        strx=strstr((const char*)buf_uart2.buf,(const char*)"+QMTSUB: 0,1,0,0");//返OK
    }
    Clear_Buffer();
	
}

//void BC28_ALYIOTSenddataAdc1(u8 *len,u8 *data)//上发数据，上发的数据跟对应的插件有关系，用户需要注意插件然后对应数据即可
//{
//    int errcount = 0;
//    memset(atstr,0,BUFLEN); //发送数据命令
//    //AT+QMTPUB=0,0,0,0,"/sys/a18dtRetCT0/BC26TEST/thing/event/property/post"
//    sprintf(atstr,"AT+QMTPUB=0,0,0,0,\"/sys/%s/%s/thing/event/property/post\"\r\n",PRODUCEKEY,DEVICENAME);
//    Uart2_SendStr(atstr);
//    delay_ms(300);
//	
//		strx=strstr((const char*)buf_uart2.buf,(const char*)">");//返SEND OK
//    while(strx==NULL)
//    {
//        errcount++;
//        delay_ms(30);
//        strx=strstr((const char*)buf_uart2.buf,(const char*)">");//返回OK
//        if(errcount>200)     //防止死循环
//        {
//            GPIO_SetBits(GPIOC,GPIO_Pin_3);		//模块重启
//            delay_ms(500);
//            GPIO_ResetBits(GPIOC,GPIO_Pin_3);
//            delay_ms(300);
//            NVIC_SystemReset();	//没有创建TCP SOCKET就重启系统等到服务器就绪
//        }
//    }	
//	
//	
//    memset(atstr,0,BUFLEN); //发送数据
//    sprintf(atstr,"{params:{IndoorTemperature:%s}}",data);
//    printf("atstr = %s \r\n",atstr);
//    Uart2_SendStr(atstr);
//    delay_ms(30);
//    UART2_send_byte(0X1A);   //发送数据
//    
//    delay_ms(30);
//    Clear_Buffer();
//}


//{"id":"123","version":"1.0","params":{"LightLux":{"value":3085},"SoilTemperature":{"value":187},"method":"thing.event.property.post"}
void BC28_ALYIOTSenddataAdc(u8 *data1,u8 *data2)//上发数据，上发的数据跟对应的插件有关系，用户需要注意插件然后对应数据即可
{
    int errcount = 0;
    memset(atstr,0,BUFLEN); //发送数据命令
    //AT+QMTPUB=0,0,0,0,"/sys/a18dtRetCT0/BC26TEST/thing/event/property/post"
    sprintf(atstr,"AT+QMTPUB=0,0,0,0,\"testtopic\"\r\n");
    Uart2_SendStr(atstr);
    delay_ms(300);
	
		strx=strstr((const char*)buf_uart2.buf,(const char*)">");//返SEND OK
    while(strx==NULL)
    {
        errcount++;
        delay_ms(30);
        strx=strstr((const char*)buf_uart2.buf,(const char*)">");//返回OK
        if(errcount>200)     //防止死循环
        {
            GPIO_SetBits(GPIOC,GPIO_Pin_3);		//模块重启
            delay_ms(500);
            GPIO_ResetBits(GPIOC,GPIO_Pin_3);
            delay_ms(300);
            NVIC_SystemReset();	//没有创建TCP SOCKET就重启系统等到服务器就绪
        }
    }	
	
	
    memset(atstr,0,BUFLEN); //发送数据 {params:{IndoorTemperature:%s,RelativeHumidity:%s}}
    sprintf(atstr,"{%s,%s}",data1,data2);
    printf("atstr = %s \r\n",atstr);
    Uart2_SendStr(atstr);
    delay_ms(30);
    UART2_send_byte(0X1A);   //发送数据
    
    delay_ms(30);
    Clear_Buffer();
}

