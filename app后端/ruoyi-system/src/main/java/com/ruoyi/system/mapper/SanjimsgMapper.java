package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Sanjimsg;

/**
 * cyjmsgMapper接口
 * 
 * @author 1
 * @date 2023-05-22
 */
public interface SanjimsgMapper 
{
    /**
     * 查询cyjmsg
     * 
     * @param way cyjmsg主键
     * @return cyjmsg
     */
    public Sanjimsg selectSanjimsgByWay(Long way);

    /**
     * 查询cyjmsg列表
     * 
     * @param sanjimsg cyjmsg
     * @return cyjmsg集合
     */
    public List<Sanjimsg> selectSanjimsgList(Sanjimsg sanjimsg);

    /**
     * 新增cyjmsg
     * 
     * @param sanjimsg cyjmsg
     * @return 结果
     */
    public int insertSanjimsg(Sanjimsg sanjimsg);

    /**
     * 修改cyjmsg
     * 
     * @param sanjimsg cyjmsg
     * @return 结果
     */
    public int updateSanjimsg(Sanjimsg sanjimsg);

    /**
     * 删除cyjmsg
     * 
     * @param way cyjmsg主键
     * @return 结果
     */
    public int deleteSanjimsgByWay(Long way);

    /**
     * 批量删除cyjmsg
     * 
     * @param ways 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSanjimsgByWays(Long[] ways);
}
