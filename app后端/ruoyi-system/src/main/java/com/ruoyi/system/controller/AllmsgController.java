package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Allmsg;
import com.ruoyi.system.service.IAllmsgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * allmsg1Controller
 * 
 * @author ruoyi
 * @date 2023-05-16
 */
@RestController
@RequestMapping("/system/allmsg")
public class AllmsgController extends BaseController
{
    @Autowired
    private IAllmsgService allmsgService;

    /**
     * 查询allmsg1列表
     */
    @PreAuthorize("@ss.hasPermi('system:allmsg:list')")
    @GetMapping("/list")
    public TableDataInfo list(Allmsg allmsg)
    {
        startPage();
        List<Allmsg> list = allmsgService.selectAllmsgList(allmsg);
        return getDataTable(list);
    }

    /**
     * 导出allmsg1列表
     */
    @PreAuthorize("@ss.hasPermi('system:allmsg:export')")
    @Log(title = "allmsg1", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Allmsg allmsg)
    {
        List<Allmsg> list = allmsgService.selectAllmsgList(allmsg);
        ExcelUtil<Allmsg> util = new ExcelUtil<Allmsg>(Allmsg.class);
        util.exportExcel(response, list, "allmsg1数据");
    }

    /**
     * 获取allmsg1详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:allmsg:query')")
    @GetMapping(value = "/{way}")
    public AjaxResult getInfo(@PathVariable("way") Long way)
    {
        return success(allmsgService.selectAllmsgByWay(way));
    }

    /**
     * 新增allmsg1
     */
    @PreAuthorize("@ss.hasPermi('system:allmsg:add')")
    @Log(title = "allmsg1", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Allmsg allmsg)
    {
        return toAjax(allmsgService.insertAllmsg(allmsg));
    }

    /**
     * 修改allmsg1
     */
    @PreAuthorize("@ss.hasPermi('system:allmsg:edit')")
    @Log(title = "allmsg1", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Allmsg allmsg)
    {
        return toAjax(allmsgService.updateAllmsg(allmsg));
    }

    /**
     * 删除allmsg1
     */
    @PreAuthorize("@ss.hasPermi('system:allmsg:remove')")
    @Log(title = "allmsg1", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ways}")
    public AjaxResult remove(@PathVariable Long[] ways)
    {
        return toAjax(allmsgService.deleteAllmsgByWays(ways));
    }
}
