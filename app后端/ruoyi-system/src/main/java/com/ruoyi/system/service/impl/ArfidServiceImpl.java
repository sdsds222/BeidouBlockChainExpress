package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ArfidMapper;
import com.ruoyi.system.domain.Arfid;
import com.ruoyi.system.service.IArfidService;

/**
 * amsgService业务层处理
 * 
 * @author 1
 * @date 2023-05-13
 */
@Service
public class ArfidServiceImpl implements IArfidService 
{
    @Autowired
    private ArfidMapper arfidMapper;

    @Override
    public Arfid selectAllByRfid(String rfid) {
        return arfidMapper.selectAllByRfid(rfid);
    }

    /**
     * 查询amsg
     * 
     * @param way amsg主键
     * @return amsg
     */
    @Override
    public Arfid selectArfidByWay(Long way)
    {
        return arfidMapper.selectArfidByWay(way);
    }

    /**
     * 查询amsg列表
     * 
     * @param arfid amsg
     * @return amsg
     */
    @Override
    public List<Arfid> selectArfidList(Arfid arfid)
    {
        return arfidMapper.selectArfidList(arfid);
    }

    /**
     * 新增amsg
     * 
     * @param arfid amsg
     * @return 结果
     */
    @Override
    public int insertArfid(Arfid arfid)
    {
        return arfidMapper.insertArfid(arfid);
    }

    /**
     * 修改amsg
     * 
     * @param arfid amsg
     * @return 结果
     */
    @Override
    public int updateArfid(Arfid arfid)
    {
        return arfidMapper.updateArfid(arfid);
    }

    /**
     * 批量删除amsg
     * 
     * @param ways 需要删除的amsg主键
     * @return 结果
     */
    @Override
    public int deleteArfidByWays(Long[] ways)
    {
        return arfidMapper.deleteArfidByWays(ways);
    }

    /**
     * 删除amsg信息
     * 
     * @param way amsg主键
     * @return 结果
     */
    @Override
    public int deleteArfidByWay(Long way)
    {
        return arfidMapper.deleteArfidByWay(way);
    }
}
