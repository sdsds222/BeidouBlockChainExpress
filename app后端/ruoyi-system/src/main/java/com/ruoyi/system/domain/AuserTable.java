package com.ruoyi.system.domain;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户对应卡号信息对象 auser_table
 * 
 * @author 1
 * @date 2023-04-28
 */

public class AuserTable extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** RFID卡信息 */
    @Excel(name = "RFID卡信息")
    private String rfid;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 条数 */
    private Long way;

    public void setRfid(String rfid)
    {
        this.rfid = rfid;
    }

    public String getRfid()
    {
        return rfid;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setWay(Long way) 
    {
        this.way = way;
    }

    public Long getWay() 
    {
        return way;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rfid", getRfid())
            .append("userId", getUserId())
            .append("username", getUsername())
            .append("way", getWay())
            .toString();
    }
}
