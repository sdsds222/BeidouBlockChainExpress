package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AuserCardMapper;
import com.ruoyi.system.domain.AuserCard;
import com.ruoyi.system.service.IAuserCardService;

/**
 * 总信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-30
 */
@Service
public class AuserCardServiceImpl implements IAuserCardService 
{
    @Autowired
    private AuserCardMapper auserCardMapper;

    /**
     * 查询总信息
     * 
     * @param way 总信息主键
     * @return 总信息
     */
    @Override
    public AuserCard selectAuserCardByWay(Long way)
    {
        return auserCardMapper.selectAuserCardByWay(way);
    }

    @Override
    public AuserCard selectAuserTableByRfid(String rfid)
    {
        return auserCardMapper.selectAuserCardByRfid(rfid);
    }

    /**
     * 查询总信息列表
     * 
     * @param auserCard 总信息
     * @return 总信息
     */
    @Override
    public List<AuserCard> selectAuserCardList(AuserCard auserCard)
    {
        return auserCardMapper.selectAuserCardList(auserCard);
    }

    /**
     * 新增总信息
     * 
     * @param auserCard 总信息
     * @return 结果
     */
    @Override
    public int insertAuserCard(AuserCard auserCard)
    {
        return auserCardMapper.insertAuserCard(auserCard);
    }

    /**
     * 修改总信息
     * 
     * @param auserCard 总信息
     * @return 结果
     */
    @Override
    public int updateAuserCard(AuserCard auserCard)
    {
        return auserCardMapper.updateAuserCard(auserCard);
    }

    /**
     * 批量删除总信息
     * 
     * @param ways 需要删除的总信息主键
     * @return 结果
     */
    @Override
    public int deleteAuserCardByWays(Long[] ways)
    {
        return auserCardMapper.deleteAuserCardByWays(ways);
    }

    /**
     * 删除总信息信息
     * 
     * @param way 总信息主键
     * @return 结果
     */
    @Override
    public int deleteAuserCardByWay(Long way)
    {
        return auserCardMapper.deleteAuserCardByWay(way);
    }
}
