package com.ruoyi.system.domain;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "file")
public class FileProperties {

    private String fileurl;
    private String filename;


    @Override
    public String toString() {
        return "FileProperties{" +
                "fileurl='" + fileurl + '\'' +
                ", filename='" + filename + '\'' +
                '}';
    }

    public String getFileurl() {
        return fileurl;
    }

    public void setFileurl(String fileurl) {
        this.fileurl = fileurl;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }



}
