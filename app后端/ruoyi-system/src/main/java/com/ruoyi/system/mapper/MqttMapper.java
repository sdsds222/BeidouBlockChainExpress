package com.ruoyi.system.mapper;


import com.ruoyi.system.domain.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MqttMapper {

      int insertSh(Sh sh);
      int insertPath(PhotoPathProperties photoPathProperties);
      PhotoPathProperties selectLastPath();
      List<Sanjimsg> getLastMsg();
      List<Sanjimsg> getAllMsg();
      List<AuserCard> getLastMsgById(String userId);
      List<Amsg> getLastLocal();
      List<Arfid> getLastrfid();
      List<Aprice> getLastprice();




}
