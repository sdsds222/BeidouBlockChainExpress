package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Sanjimsg;
import com.ruoyi.system.service.ISanjimsgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * cyjmsgController
 * 
 * @author 1
 * @date 2023-05-22
 */
@RestController
@RequestMapping("/system/sanjimsg")
public class SanjimsgController extends BaseController
{
    @Autowired
    private ISanjimsgService sanjimsgService;

    /**
     * 查询cyjmsg列表
     */
    @PreAuthorize("@ss.hasPermi('system:sanjimsg:list')")
    @GetMapping("/list")
    public TableDataInfo list(Sanjimsg sanjimsg)
    {
        startPage();
        List<Sanjimsg> list = sanjimsgService.selectSanjimsgList(sanjimsg);
        return getDataTable(list);
    }

    /**
     * 导出cyjmsg列表
     */
    @PreAuthorize("@ss.hasPermi('system:sanjimsg:export')")
    @Log(title = "cyjmsg", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Sanjimsg sanjimsg)
    {
        List<Sanjimsg> list = sanjimsgService.selectSanjimsgList(sanjimsg);
        ExcelUtil<Sanjimsg> util = new ExcelUtil<Sanjimsg>(Sanjimsg.class);
        util.exportExcel(response, list, "cyjmsg数据");
    }

    /**
     * 获取cyjmsg详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:sanjimsg:query')")
    @GetMapping(value = "/{way}")
    public AjaxResult getInfo(@PathVariable("way") Long way)
    {
        return success(sanjimsgService.selectSanjimsgByWay(way));
    }

    /**
     * 新增cyjmsg
     */
    @PreAuthorize("@ss.hasPermi('system:sanjimsg:add')")
    @Log(title = "cyjmsg", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Sanjimsg sanjimsg)
    {
        return toAjax(sanjimsgService.insertSanjimsg(sanjimsg));
    }

    /**
     * 修改cyjmsg
     */
    @PreAuthorize("@ss.hasPermi('system:sanjimsg:edit')")
    @Log(title = "cyjmsg", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Sanjimsg sanjimsg)
    {
        return toAjax(sanjimsgService.updateSanjimsg(sanjimsg));
    }

    /**
     * 删除cyjmsg
     */
    @PreAuthorize("@ss.hasPermi('system:sanjimsg:remove')")
    @Log(title = "cyjmsg", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ways}")
    public AjaxResult remove(@PathVariable Long[] ways)
    {
        return toAjax(sanjimsgService.deleteSanjimsgByWays(ways));
    }
}
