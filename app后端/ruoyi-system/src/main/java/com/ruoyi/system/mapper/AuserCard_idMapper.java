package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AuserCard_id;
import com.ruoyi.system.domain.AuserTable;

/**
 * msgId联查Mapper接口
 * 
 * @author 2
 * @date 2023-04-28
 */
public interface AuserCard_idMapper 
{
    /**
     * 查询msgId联查
     * 
     * @param way msgId联查主键
     * @return msgId联查
     */
    public AuserCard_id selectAuserCard_idByWay(Long way);

    /**
     * 查询msgId联查列表
     * 
     * @param auserCard_id msgId联查
     * @return msgId联查集合
     */
    public List<AuserCard_id> selectAuserCard_idList(AuserCard_id auserCard_id);

    /**
     * 新增msgId联查
     * 
     * @param auserCard_id msgId联查
     * @return 结果
     */
    public int insertAuserCard_id(AuserCard_id auserCard_id);

    /**
     * 修改msgId联查
     * 
     * @param auserCard_id msgId联查
     * @return 结果
     */
    public int updateAuserCard_id(AuserCard_id auserCard_id);

    /**
     * 删除msgId联查
     * 
     * @param way msgId联查主键
     * @return 结果
     */
    public int deleteAuserCard_idByWay(Long way);

    /**
     * 批量删除msgId联查
     * 
     * @param ways 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAuserCard_idByWays(Long[] ways);

    /**
     * 批量删除用户对应卡号信息
     * 
     * @param ways 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAuserTableByUserIds(Long[] ways);
    
    /**
     * 批量新增用户对应卡号信息
     * 
     * @param auserTableList 用户对应卡号信息列表
     * @return 结果
     */
    public int batchAuserTable(List<AuserTable> auserTableList);
    

    /**
     * 通过msgId联查主键删除用户对应卡号信息信息
     * 
     * @param way msgId联查ID
     * @return 结果
     */
    public int deleteAuserTableByUserId(Long way);
}
