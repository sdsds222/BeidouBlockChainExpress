package com.ruoyi.system.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * msgId联查对象 auser_card_id
 * 
 * @author 2
 * @date 2023-04-28
 */
public class AuserCard_id extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** rfid卡号 */
    @Excel(name = "rfid卡号")
    private String rfid;

    /** 用户Id */
    @Excel(name = "用户Id")
    private Long userId;

    /** 充值 */
    @Excel(name = "充值")
    private String cz;

    /** 消费 */
    @Excel(name = "消费")
    private String xf;

    /** 余额 */
    @Excel(name = "余额")
    private String ye;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateTime;

    /** 条数 */
    private Long way;

    /** 用户对应卡号信息信息 */
    private List<AuserTable> auserTableList;

    public void setRfid(String rfid) 
    {
        this.rfid = rfid;
    }

    public String getRfid() 
    {
        return rfid;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setCz(String cz) 
    {
        this.cz = cz;
    }

    public String getCz() 
    {
        return cz;
    }
    public void setXf(String xf) 
    {
        this.xf = xf;
    }

    public String getXf() 
    {
        return xf;
    }
    public void setYe(String ye) 
    {
        this.ye = ye;
    }

    public String getYe() 
    {
        return ye;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDateTime(Date dateTime) 
    {
        this.dateTime = dateTime;
    }

    public Date getDateTime() 
    {
        return dateTime;
    }
    public void setWay(Long way) 
    {
        this.way = way;
    }

    public Long getWay() 
    {
        return way;
    }

    public List<AuserTable> getAuserTableList()
    {
        return auserTableList;
    }

    public void setAuserTableList(List<AuserTable> auserTableList)
    {
        this.auserTableList = auserTableList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rfid", getRfid())
            .append("userId", getUserId())
            .append("cz", getCz())
            .append("xf", getXf())
            .append("ye", getYe())
            .append("status", getStatus())
            .append("dateTime", getDateTime())
            .append("way", getWay())
            .append("auserTableList", getAuserTableList())
            .toString();
    }
}
