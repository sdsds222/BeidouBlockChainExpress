package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Aprice;
import com.ruoyi.system.service.IApriceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * priceController
 * 
 * @author t1
 * @date 2023-05-13
 */
@RestController
@RequestMapping("/system/aprice")
public class ApriceController extends BaseController
{
    @Autowired
    private IApriceService apriceService;

    /**
     * 查询price列表
     */
    @PreAuthorize("@ss.hasPermi('system:aprice:list')")
    @GetMapping("/list")
    public TableDataInfo list(Aprice aprice)
    {
        startPage();
        List<Aprice> list = apriceService.selectApriceList(aprice);
        return getDataTable(list);
    }

    /**
     * 导出price列表
     */
    @PreAuthorize("@ss.hasPermi('system:aprice:export')")
    @Log(title = "price", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Aprice aprice)
    {
        List<Aprice> list = apriceService.selectApriceList(aprice);
        ExcelUtil<Aprice> util = new ExcelUtil<Aprice>(Aprice.class);
        util.exportExcel(response, list, "price数据");
    }

    /**
     * 获取price详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:aprice:query')")
    @GetMapping(value = "/{way}")
    public AjaxResult getInfo(@PathVariable("way") Long way)
    {
        return success(apriceService.selectApriceByWay(way));
    }

    /**
     * 新增price
     */
    @PreAuthorize("@ss.hasPermi('system:aprice:add')")
    @Log(title = "price", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Aprice aprice)
    {
        return toAjax(apriceService.insertAprice(aprice));
    }

    /**
     * 修改price
     */
    @PreAuthorize("@ss.hasPermi('system:aprice:edit')")
    @Log(title = "price", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Aprice aprice)
    {
        return toAjax(apriceService.updateAprice(aprice));
    }

    /**
     * 删除price
     */
    @PreAuthorize("@ss.hasPermi('system:aprice:remove')")
    @Log(title = "price", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ways}")
    public AjaxResult remove(@PathVariable Long[] ways)
    {
        return toAjax(apriceService.deleteApriceByWays(ways));
    }
}
