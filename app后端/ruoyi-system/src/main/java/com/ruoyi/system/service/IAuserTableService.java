package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AuserTable;

/**
 * 用户对应卡号信息Service接口
 * 
 * @author 1
 * @date 2023-04-28
 */
public interface IAuserTableService 
{
    /**
     * 查询用户对应卡号信息
     * 
     * @param way 用户对应卡号信息主键
     * @return 用户对应卡号信息
     */
    public AuserTable selectAuserTableByWay(Long way);
    public AuserTable selectAuserTableByRfid(String rfid);
    /**
     * 查询用户对应卡号信息列表
     * 
     * @param auserTable 用户对应卡号信息
     * @return 用户对应卡号信息集合
     */
    public List<AuserTable> selectAuserTableList(AuserTable auserTable);

    /**
     * 新增用户对应卡号信息
     * 
     * @param auserTable 用户对应卡号信息
     * @return 结果
     */
    public int insertAuserTable(AuserTable auserTable);

    /**
     * 修改用户对应卡号信息
     * 
     * @param auserTable 用户对应卡号信息
     * @return 结果
     */
    public int updateAuserTable(AuserTable auserTable);

    /**
     * 批量删除用户对应卡号信息
     * 
     * @param ways 需要删除的用户对应卡号信息主键集合
     * @return 结果
     */
    public int deleteAuserTableByWays(Long[] ways);

    /**
     * 删除用户对应卡号信息信息
     * 
     * @param way 用户对应卡号信息主键
     * @return 结果
     */
    public int deleteAuserTableByWay(Long way);
}
