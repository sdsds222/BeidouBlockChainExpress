package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * cyjmsg对象 sanjimsg
 * 
 * @author 1
 * @date 2023-05-22
 */
public class Sanjimsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    @Excel(name = "")
    private String hr;

    /**  */
    @Excel(name = "")
    private String spo2;

    /**  */
    @Excel(name = "")
    private String lat;

    /**  */
    @Excel(name = "")
    private String lng;

    /**  */
    @Excel(name = "")
    private String status;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateTime;

    /**  */
    private Long way;

    public void setHr(String hr) 
    {
        this.hr = hr;
    }

    public String getHr() 
    {
        return hr;
    }
    public void setSpo2(String spo2) 
    {
        this.spo2 = spo2;
    }

    public String getSpo2() 
    {
        return spo2;
    }
    public void setLat(String lat) 
    {
        this.lat = lat;
    }

    public String getLat() 
    {
        return lat;
    }
    public void setLng(String lng) 
    {
        this.lng = lng;
    }

    public String getLng() 
    {
        return lng;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDateTime(Date dateTime) 
    {
        this.dateTime = dateTime;
    }

    public Date getDateTime() 
    {
        return dateTime;
    }
    public void setWay(Long way) 
    {
        this.way = way;
    }

    public Long getWay() 
    {
        return way;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("hr", getHr())
            .append("spo2", getSpo2())
            .append("lat", getLat())
            .append("lng", getLng())
            .append("status", getStatus())
            .append("dateTime", getDateTime())
            .append("way", getWay())
            .toString();
    }
}
