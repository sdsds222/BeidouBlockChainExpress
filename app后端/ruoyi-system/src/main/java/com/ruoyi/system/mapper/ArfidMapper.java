package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Arfid;

/**
 * amsgMapper接口
 * 
 * @author 1
 * @date 2023-05-13
 */
public interface ArfidMapper 
{
    /**
     * 查询amsg
     * 
     * @param way amsg主键
     * @return amsg
     */
     public Arfid selectAllByRfid(String rfid);
    public Arfid selectArfidByWay(Long way);

    /**
     * 查询amsg列表
     * 
     * @param arfid amsg
     * @return amsg集合
     */
    public List<Arfid> selectArfidList(Arfid arfid);

    /**
     * 新增amsg
     * 
     * @param arfid amsg
     * @return 结果
     */
    public int insertArfid(Arfid arfid);

    /**
     * 修改amsg
     * 
     * @param arfid amsg
     * @return 结果
     */
    public int updateArfid(Arfid arfid);

    /**
     * 删除amsg
     * 
     * @param way amsg主键
     * @return 结果
     */
    public int deleteArfidByWay(Long way);

    /**
     * 批量删除amsg
     * 
     * @param ways 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteArfidByWays(Long[] ways);
}
