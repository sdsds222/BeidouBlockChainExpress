package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.AuserCard_id;
import com.ruoyi.system.service.IAuserCard_idService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * msgId联查Controller
 * 
 * @author 2
 * @date 2023-04-28
 */
@RestController
@RequestMapping("/system/card3")
public class AuserCard_idController extends BaseController
{
    @Autowired
    private IAuserCard_idService auserCard_idService;

    /**
     * 查询msgId联查列表
     */
    @PreAuthorize("@ss.hasPermi('system:card3:list')")
    @GetMapping("/list")
    public TableDataInfo list(AuserCard_id auserCard_id)
    {
        startPage();
        List<AuserCard_id> list = auserCard_idService.selectAuserCard_idList(auserCard_id);
        return getDataTable(list);
    }

    /**
     * 导出msgId联查列表
     */
    @PreAuthorize("@ss.hasPermi('system:card3:export')")
    @Log(title = "msgId联查", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AuserCard_id auserCard_id)
    {
        List<AuserCard_id> list = auserCard_idService.selectAuserCard_idList(auserCard_id);
        ExcelUtil<AuserCard_id> util = new ExcelUtil<AuserCard_id>(AuserCard_id.class);
        util.exportExcel(response, list, "msgId联查数据");
    }

    /**
     * 获取msgId联查详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:card3:query')")
    @GetMapping(value = "/{way}")
    public AjaxResult getInfo(@PathVariable("way") Long way)
    {
        return success(auserCard_idService.selectAuserCard_idByWay(way));
    }

    /**
     * 新增msgId联查
     */
    @PreAuthorize("@ss.hasPermi('system:card3:add')")
    @Log(title = "msgId联查", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AuserCard_id auserCard_id)
    {
        return toAjax(auserCard_idService.insertAuserCard_id(auserCard_id));
    }

    /**
     * 修改msgId联查
     */
    @PreAuthorize("@ss.hasPermi('system:card3:edit')")
    @Log(title = "msgId联查", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AuserCard_id auserCard_id)
    {
        return toAjax(auserCard_idService.updateAuserCard_id(auserCard_id));
    }

    /**
     * 删除msgId联查
     */
    @PreAuthorize("@ss.hasPermi('system:card3:remove')")
    @Log(title = "msgId联查", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ways}")
    public AjaxResult remove(@PathVariable Long[] ways)
    {
        return toAjax(auserCard_idService.deleteAuserCard_idByWays(ways));
    }
}
