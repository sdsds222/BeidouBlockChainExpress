package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Allmsg;

/**
 * allmsg1Mapper接口
 * 
 * @author ruoyi
 * @date 2023-05-16
 */
public interface AllmsgMapper 
{
    /**
     * 查询allmsg1
     * 
     * @param way allmsg1主键
     * @return allmsg1
     */
    public Allmsg selectAllmsgByWay(Long way);

    /**
     * 查询allmsg1列表
     * 
     * @param allmsg allmsg1
     * @return allmsg1集合
     */
    public List<Allmsg> selectAllmsgList(Allmsg allmsg);

    /**
     * 新增allmsg1
     * 
     * @param allmsg allmsg1
     * @return 结果
     */
    public int insertAllmsg(Allmsg allmsg);

    /**
     * 修改allmsg1
     * 
     * @param allmsg allmsg1
     * @return 结果
     */
    public int updateAllmsg(Allmsg allmsg);

    /**
     * 删除allmsg1
     * 
     * @param way allmsg1主键
     * @return 结果
     */
    public int deleteAllmsgByWay(Long way);

    /**
     * 批量删除allmsg1
     * 
     * @param ways 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAllmsgByWays(Long[] ways);
}
