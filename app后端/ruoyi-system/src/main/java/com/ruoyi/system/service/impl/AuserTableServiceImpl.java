package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AuserTableMapper;
import com.ruoyi.system.domain.AuserTable;
import com.ruoyi.system.service.IAuserTableService;

/**
 * 用户对应卡号信息Service业务层处理
 * 
 * @author 1
 * @date 2023-04-28
 */
@Service
public class AuserTableServiceImpl implements IAuserTableService 
{
    @Autowired
    private AuserTableMapper auserTableMapper;

    /**
     * 查询用户对应卡号信息
     * 
     * @param way 用户对应卡号信息主键
     * @return 用户对应卡号信息
     */
    @Override
    public AuserTable selectAuserTableByWay(Long way)
    {
        return auserTableMapper.selectAuserTableByWay(way);
    }
    @Override
    public AuserTable selectAuserTableByRfid(String rfid)
    {
        return auserTableMapper.selectAuserTableByRfid(rfid);
    }

    /**
     * 查询用户对应卡号信息列表
     * 
     * @param auserTable 用户对应卡号信息
     * @return 用户对应卡号信息
     */
    @Override
    public List<AuserTable> selectAuserTableList(AuserTable auserTable)
    {
        return auserTableMapper.selectAuserTableList(auserTable);
    }

    /**
     * 新增用户对应卡号信息
     * 
     * @param auserTable 用户对应卡号信息
     * @return 结果
     */
    @Override
    public int insertAuserTable(AuserTable auserTable)
    {
        return auserTableMapper.insertAuserTable(auserTable);
    }

    /**
     * 修改用户对应卡号信息
     * 
     * @param auserTable 用户对应卡号信息
     * @return 结果
     */
    @Override
    public int updateAuserTable(AuserTable auserTable)
    {
        return auserTableMapper.updateAuserTable(auserTable);
    }

    /**
     * 批量删除用户对应卡号信息
     * 
     * @param ways 需要删除的用户对应卡号信息主键
     * @return 结果
     */
    @Override
    public int deleteAuserTableByWays(Long[] ways)
    {
        return auserTableMapper.deleteAuserTableByWays(ways);
    }

    /**
     * 删除用户对应卡号信息信息
     * 
     * @param way 用户对应卡号信息主键
     * @return 结果
     */
    @Override
    public int deleteAuserTableByWay(Long way)
    {
        return auserTableMapper.deleteAuserTableByWay(way);
    }
}
