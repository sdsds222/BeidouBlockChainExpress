package com.ruoyi.system.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Sh {
    private String ShgroupsId;
    private String ShgroupsName;
    private String status;
    private String floor;
    private String dateTime;
}
