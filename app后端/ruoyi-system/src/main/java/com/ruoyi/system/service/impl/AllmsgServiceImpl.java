package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AllmsgMapper;
import com.ruoyi.system.domain.Allmsg;
import com.ruoyi.system.service.IAllmsgService;

/**
 * allmsg1Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-16
 */
@Service
public class AllmsgServiceImpl implements IAllmsgService 
{
    @Autowired
    private AllmsgMapper allmsgMapper;

    /**
     * 查询allmsg1
     * 
     * @param way allmsg1主键
     * @return allmsg1
     */
    @Override
    public Allmsg selectAllmsgByWay(Long way)
    {
        return allmsgMapper.selectAllmsgByWay(way);
    }

    /**
     * 查询allmsg1列表
     * 
     * @param allmsg allmsg1
     * @return allmsg1
     */
    @Override
    public List<Allmsg> selectAllmsgList(Allmsg allmsg)
    {
        return allmsgMapper.selectAllmsgList(allmsg);
    }

    /**
     * 新增allmsg1
     * 
     * @param allmsg allmsg1
     * @return 结果
     */
    @Override
    public int insertAllmsg(Allmsg allmsg)
    {
        return allmsgMapper.insertAllmsg(allmsg);
    }

    /**
     * 修改allmsg1
     * 
     * @param allmsg allmsg1
     * @return 结果
     */
    @Override
    public int updateAllmsg(Allmsg allmsg)
    {
        return allmsgMapper.updateAllmsg(allmsg);
    }

    /**
     * 批量删除allmsg1
     * 
     * @param ways 需要删除的allmsg1主键
     * @return 结果
     */
    @Override
    public int deleteAllmsgByWays(Long[] ways)
    {
        return allmsgMapper.deleteAllmsgByWays(ways);
    }

    /**
     * 删除allmsg1信息
     * 
     * @param way allmsg1主键
     * @return 结果
     */
    @Override
    public int deleteAllmsgByWay(Long way)
    {
        return allmsgMapper.deleteAllmsgByWay(way);
    }
}
