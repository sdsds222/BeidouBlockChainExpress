package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * amsg对象 amsg
 * 
 * @author t1
 * @date 2023-05-13
 */
public class Amsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 温度 */
    private String temp;

    /** 转速 */
    private String rpm;

    /** 经度 */
    private String lng;

    /** 维度 */
    private String lat;

    /** 时间 */
    private Date dateTime;

    /** 条数 */
    private Long way;
    /** 电量 */
    private String voltage;

    public void setTemp(String temp) 
    {
        this.temp = temp;
    }

    public String getTemp() 
    {
        return temp;
    }
    public void setRpm(String rpm) 
    {
        this.rpm = rpm;
    }

    public String getRpm() 
    {
        return rpm;
    }
    public void setLng(String lng) 
    {
        this.lng = lng;
    }

    public String getLng() 
    {
        return lng;
    }
    public void setLat(String lat) 
    {
        this.lat = lat;
    }

    public String getLat() 
    {
        return lat;
    }
    public void setDateTime(Date dateTime) 
    {
        this.dateTime = dateTime;
    }

    public Date getDateTime() 
    {
        return dateTime;
    }
    public void setWay(Long way) 
    {
        this.way = way;
    }

    public Long getWay() 
    {
        return way;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("temp", getTemp())
            .append("rpm", getRpm())
            .append("lng", getLng())
            .append("lat", getLat())
            .append("dateTime", getDateTime())
            .append("way", getWay())
            .toString();
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }
}
