package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Amsg;

/**
 * amsgMapper接口
 * 
 * @author t1
 * @date 2023-05-13
 */
public interface AmsgMapper 
{
    /**
     * 查询amsg
     * 
     * @param way amsg主键
     * @return amsg
     */
    public Amsg selectAmsgByWay(Long way);

    /**
     * 查询amsg列表
     * 
     * @param amsg amsg
     * @return amsg集合
     */
    public List<Amsg> selectAmsgList(Amsg amsg);

    /**
     * 新增amsg
     * 
     * @param amsg amsg
     * @return 结果
     */
    public int insertAmsg(Amsg amsg);

    /**
     * 修改amsg
     * 
     * @param amsg amsg
     * @return 结果
     */
    public int updateAmsg(Amsg amsg);

    /**
     * 删除amsg
     * 
     * @param way amsg主键
     * @return 结果
     */
    public int deleteAmsgByWay(Long way);

    /**
     * 批量删除amsg
     * 
     * @param ways 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAmsgByWays(Long[] ways);
}
