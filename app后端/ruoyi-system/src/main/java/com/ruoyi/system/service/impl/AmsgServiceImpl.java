package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AmsgMapper;
import com.ruoyi.system.domain.Amsg;
import com.ruoyi.system.service.IAmsgService;

/**
 * amsgService业务层处理
 * 
 * @author t1
 * @date 2023-05-13
 */
@Service
public class AmsgServiceImpl implements IAmsgService 
{
    @Autowired
    private AmsgMapper amsgMapper;

    /**
     * 查询amsg
     * 
     * @param way amsg主键
     * @return amsg
     */
    @Override
    public Amsg selectAmsgByWay(Long way)
    {
        return amsgMapper.selectAmsgByWay(way);
    }

    /**
     * 查询amsg列表
     * 
     * @param amsg amsg
     * @return amsg
     */
    @Override
    public List<Amsg> selectAmsgList(Amsg amsg)
    {
        return amsgMapper.selectAmsgList(amsg);
    }

    /**
     * 新增amsg
     * 
     * @param amsg amsg
     * @return 结果
     */
    @Override
    public int insertAmsg(Amsg amsg)
    {
        return amsgMapper.insertAmsg(amsg);
    }

    /**
     * 修改amsg
     * 
     * @param amsg amsg
     * @return 结果
     */
    @Override
    public int updateAmsg(Amsg amsg)
    {
        return amsgMapper.updateAmsg(amsg);
    }

    /**
     * 批量删除amsg
     * 
     * @param ways 需要删除的amsg主键
     * @return 结果
     */
    @Override
    public int deleteAmsgByWays(Long[] ways)
    {
        return amsgMapper.deleteAmsgByWays(ways);
    }

    /**
     * 删除amsg信息
     * 
     * @param way amsg主键
     * @return 结果
     */
    @Override
    public int deleteAmsgByWay(Long way)
    {
        return amsgMapper.deleteAmsgByWay(way);
    }
}
