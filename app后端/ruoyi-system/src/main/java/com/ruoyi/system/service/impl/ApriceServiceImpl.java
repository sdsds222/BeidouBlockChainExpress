package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApriceMapper;
import com.ruoyi.system.domain.Aprice;
import com.ruoyi.system.service.IApriceService;

/**
 * priceService业务层处理
 * 
 * @author t1
 * @date 2023-05-13
 */
@Service
public class ApriceServiceImpl implements IApriceService 
{
    @Autowired
    private ApriceMapper apriceMapper;

    /**
     * 查询price
     * 
     * @param way price主键
     * @return price
     */
    @Override
    public Aprice selectApriceByWay(Long way)
    {
        return apriceMapper.selectApriceByWay(way);
    }

    /**
     * 查询price列表
     * 
     * @param aprice price
     * @return price
     */
    @Override
    public List<Aprice> selectApriceList(Aprice aprice)
    {
        return apriceMapper.selectApriceList(aprice);
    }

    /**
     * 新增price
     * 
     * @param aprice price
     * @return 结果
     */
    @Override
    public int insertAprice(Aprice aprice)
    {
        return apriceMapper.insertAprice(aprice);
    }

    /**
     * 修改price
     * 
     * @param aprice price
     * @return 结果
     */
    @Override
    public int updateAprice(Aprice aprice)
    {
        return apriceMapper.updateAprice(aprice);
    }

    /**
     * 批量删除price
     * 
     * @param ways 需要删除的price主键
     * @return 结果
     */
    @Override
    public int deleteApriceByWays(Long[] ways)
    {
        return apriceMapper.deleteApriceByWays(ways);
    }

    /**
     * 删除price信息
     * 
     * @param way price主键
     * @return 结果
     */
    @Override
    public int deleteApriceByWay(Long way)
    {
        return apriceMapper.deleteApriceByWay(way);
    }
}
