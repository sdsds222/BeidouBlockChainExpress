package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.system.domain.AuserTable;
import com.ruoyi.system.mapper.AuserCard_idMapper;
import com.ruoyi.system.domain.AuserCard_id;
import com.ruoyi.system.service.IAuserCard_idService;

/**
 * msgId联查Service业务层处理
 * 
 * @author 2
 * @date 2023-04-28
 */
@Service
public class AuserCard_idServiceImpl implements IAuserCard_idService 
{
    @Autowired
    private AuserCard_idMapper auserCard_idMapper;

    /**
     * 查询msgId联查
     * 
     * @param way msgId联查主键
     * @return msgId联查
     */
    @Override
    public AuserCard_id selectAuserCard_idByWay(Long way)
    {
        return auserCard_idMapper.selectAuserCard_idByWay(way);
    }

    /**
     * 查询msgId联查列表
     * 
     * @param auserCard_id msgId联查
     * @return msgId联查
     */
    @Override
    public List<AuserCard_id> selectAuserCard_idList(AuserCard_id auserCard_id)
    {
        return auserCard_idMapper.selectAuserCard_idList(auserCard_id);
    }

    /**
     * 新增msgId联查
     * 
     * @param auserCard_id msgId联查
     * @return 结果
     */
    @Transactional
    @Override
    public int insertAuserCard_id(AuserCard_id auserCard_id)
    {
        int rows = auserCard_idMapper.insertAuserCard_id(auserCard_id);
        insertAuserTable(auserCard_id);
        return rows;
    }

    /**
     * 修改msgId联查
     * 
     * @param auserCard_id msgId联查
     * @return 结果
     */
    @Transactional
    @Override
    public int updateAuserCard_id(AuserCard_id auserCard_id)
    {
        auserCard_idMapper.deleteAuserTableByUserId(auserCard_id.getWay());
        insertAuserTable(auserCard_id);
        return auserCard_idMapper.updateAuserCard_id(auserCard_id);
    }

    /**
     * 批量删除msgId联查
     * 
     * @param ways 需要删除的msgId联查主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteAuserCard_idByWays(Long[] ways)
    {
        auserCard_idMapper.deleteAuserTableByUserIds(ways);
        return auserCard_idMapper.deleteAuserCard_idByWays(ways);
    }

    /**
     * 删除msgId联查信息
     * 
     * @param way msgId联查主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteAuserCard_idByWay(Long way)
    {
        auserCard_idMapper.deleteAuserTableByUserId(way);
        return auserCard_idMapper.deleteAuserCard_idByWay(way);
    }

    /**
     * 新增用户对应卡号信息信息
     * 
     * @param auserCard_id msgId联查对象
     */
    public void insertAuserTable(AuserCard_id auserCard_id)
    {
        List<AuserTable> auserTableList = auserCard_id.getAuserTableList();
        Long way = auserCard_id.getWay();
        if (StringUtils.isNotNull(auserTableList))
        {
            List<AuserTable> list = new ArrayList<AuserTable>();
            for (AuserTable auserTable : auserTableList)
            {
                auserTable.setUserId(way);
                list.add(auserTable);
            }
            if (list.size() > 0)
            {
                auserCard_idMapper.batchAuserTable(list);
            }
        }
    }
}
