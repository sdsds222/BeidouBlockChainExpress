package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Arfid;
import com.ruoyi.system.service.IArfidService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * amsgController
 * 
 * @author 1
 * @date 2023-05-13
 */
@RestController
@RequestMapping("/system/arfid")
public class ArfidController extends BaseController
{
    @Autowired
    private IArfidService arfidService;

    /**
     * 查询amsg列表
     */
    @PreAuthorize("@ss.hasPermi('system:arfid:list')")
    @GetMapping("/list")
    public TableDataInfo list(Arfid arfid)
    {
        startPage();
        List<Arfid> list = arfidService.selectArfidList(arfid);
        return getDataTable(list);
    }

    /**
     * 导出amsg列表
     */
    @PreAuthorize("@ss.hasPermi('system:arfid:export')")
    @Log(title = "amsg", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Arfid arfid)
    {
        List<Arfid> list = arfidService.selectArfidList(arfid);
        ExcelUtil<Arfid> util = new ExcelUtil<Arfid>(Arfid.class);
        util.exportExcel(response, list, "amsg数据");
    }

    /**
     * 获取amsg详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:arfid:query')")
    @GetMapping(value = "/{way}")
    public AjaxResult getInfo(@PathVariable("way") Long way)
    {
        return success(arfidService.selectArfidByWay(way));
    }

    /**
     * 新增amsg
     */
    @PreAuthorize("@ss.hasPermi('system:arfid:add')")
    @Log(title = "amsg", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Arfid arfid)
    {
        return toAjax(arfidService.insertArfid(arfid));
    }

    /**
     * 修改amsg
     */
    @PreAuthorize("@ss.hasPermi('system:arfid:edit')")
    @Log(title = "amsg", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Arfid arfid)
    {
        return toAjax(arfidService.updateArfid(arfid));
    }

    /**
     * 删除amsg
     */
    @PreAuthorize("@ss.hasPermi('system:arfid:remove')")
    @Log(title = "amsg", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ways}")
    public AjaxResult remove(@PathVariable Long[] ways)
    {
        return toAjax(arfidService.deleteArfidByWays(ways));
    }
}
