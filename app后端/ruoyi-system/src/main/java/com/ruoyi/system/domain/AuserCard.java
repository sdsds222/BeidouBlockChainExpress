package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 总信息对象 auser_card
 * 
 * @author ruoyi
 * @date 2023-04-30
 */

public class AuserCard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** RFID卡号 */
    @Excel(name = "RFID卡号")
    private String rfid;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 存款 */
    @Excel(name = "存款")
    private String cz;

    /** 消费 */
    @Excel(name = "消费")
    private String xf;

    /** 余额 */
    @Excel(name = "余额")
    private String ye;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 记录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateTime;

    /** $column.columnComment */
    private Long way;

    public void setRfid(String rfid) 
    {
        this.rfid = rfid;
    }

    public String getRfid() 
    {
        return rfid;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setCz(String cz) 
    {
        this.cz = cz;
    }

    public String getCz() 
    {
        return cz;
    }
    public void setXf(String xf) 
    {
        this.xf = xf;
    }

    public String getXf() 
    {
        return xf;
    }
    public void setYe(String ye) 
    {
        this.ye = ye;
    }

    public String getYe() 
    {
        return ye;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDateTime(Date dateTime) 
    {
        this.dateTime = dateTime;
    }

    public Date getDateTime() 
    {
        return dateTime;
    }
    public void setWay(Long way) 
    {
        this.way = way;
    }

    public Long getWay() 
    {
        return way;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rfid", getRfid())
            .append("userId", getUserId())
            .append("username", getUsername())
            .append("cz", getCz())
            .append("xf", getXf())
            .append("ye", getYe())
            .append("status", getStatus())
            .append("dateTime", getDateTime())
            .append("way", getWay())
            .toString();
    }
}
