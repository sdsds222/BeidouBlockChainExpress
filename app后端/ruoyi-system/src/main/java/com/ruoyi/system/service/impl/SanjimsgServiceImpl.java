package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SanjimsgMapper;
import com.ruoyi.system.domain.Sanjimsg;
import com.ruoyi.system.service.ISanjimsgService;

/**
 * cyjmsgService业务层处理
 * 
 * @author 1
 * @date 2023-05-22
 */
@Service
public class SanjimsgServiceImpl implements ISanjimsgService 
{
    @Autowired
    private SanjimsgMapper sanjimsgMapper;

    /**
     * 查询cyjmsg
     * 
     * @param way cyjmsg主键
     * @return cyjmsg
     */
    @Override
    public Sanjimsg selectSanjimsgByWay(Long way)
    {
        return sanjimsgMapper.selectSanjimsgByWay(way);
    }

    /**
     * 查询cyjmsg列表
     * 
     * @param sanjimsg cyjmsg
     * @return cyjmsg
     */
    @Override
    public List<Sanjimsg> selectSanjimsgList(Sanjimsg sanjimsg)
    {
        return sanjimsgMapper.selectSanjimsgList(sanjimsg);
    }

    /**
     * 新增cyjmsg
     * 
     * @param sanjimsg cyjmsg
     * @return 结果
     */
    @Override
    public int insertSanjimsg(Sanjimsg sanjimsg)
    {
        return sanjimsgMapper.insertSanjimsg(sanjimsg);
    }

    /**
     * 修改cyjmsg
     * 
     * @param sanjimsg cyjmsg
     * @return 结果
     */
    @Override
    public int updateSanjimsg(Sanjimsg sanjimsg)
    {
        return sanjimsgMapper.updateSanjimsg(sanjimsg);
    }

    /**
     * 批量删除cyjmsg
     * 
     * @param ways 需要删除的cyjmsg主键
     * @return 结果
     */
    @Override
    public int deleteSanjimsgByWays(Long[] ways)
    {
        return sanjimsgMapper.deleteSanjimsgByWays(ways);
    }

    /**
     * 删除cyjmsg信息
     * 
     * @param way cyjmsg主键
     * @return 结果
     */
    @Override
    public int deleteSanjimsgByWay(Long way)
    {
        return sanjimsgMapper.deleteSanjimsgByWay(way);
    }
}
