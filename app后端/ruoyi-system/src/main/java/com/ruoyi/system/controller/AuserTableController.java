package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.AuserTable;
import com.ruoyi.system.service.IAuserTableService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户对应卡号信息Controller
 * 
 * @author 1
 * @date 2023-04-28
 */
@RestController
@RequestMapping("/system/card2")
public class AuserTableController extends BaseController
{
    @Autowired
    private IAuserTableService auserTableService;

    /**
     * 查询用户对应卡号信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:card2:list')")
    @GetMapping("/list")
    public TableDataInfo list(AuserTable auserTable)
    {
        startPage();
        List<AuserTable> list = auserTableService.selectAuserTableList(auserTable);
        return getDataTable(list);
    }

    /**
     * 导出用户对应卡号信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:card2:export')")
    @Log(title = "用户对应卡号信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AuserTable auserTable)
    {
        List<AuserTable> list = auserTableService.selectAuserTableList(auserTable);
        ExcelUtil<AuserTable> util = new ExcelUtil<AuserTable>(AuserTable.class);
        util.exportExcel(response, list, "用户对应卡号信息数据");
    }

    /**
     * 获取用户对应卡号信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:card2:query')")
    @GetMapping(value = "/{way}")
    public AjaxResult getInfo(@PathVariable("way") Long way)
    {
        return success(auserTableService.selectAuserTableByWay(way));
    }

    /**
     * 新增用户对应卡号信息
     */
    @PreAuthorize("@ss.hasPermi('system:card2:add')")
    @Log(title = "用户对应卡号信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AuserTable auserTable)
    {
        return toAjax(auserTableService.insertAuserTable(auserTable));
    }

    /**
     * 修改用户对应卡号信息
     */
    @PreAuthorize("@ss.hasPermi('system:card2:edit')")
    @Log(title = "用户对应卡号信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AuserTable auserTable)
    {
        return toAjax(auserTableService.updateAuserTable(auserTable));
    }

    /**
     * 删除用户对应卡号信息
     */
    @PreAuthorize("@ss.hasPermi('system:card2:remove')")
    @Log(title = "用户对应卡号信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ways}")
    public AjaxResult remove(@PathVariable Long[] ways)
    {
        return toAjax(auserTableService.deleteAuserTableByWays(ways));
    }
}
