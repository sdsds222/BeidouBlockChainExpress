package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Amsg;
import com.ruoyi.system.service.IAmsgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * amsgController
 * 
 * @author t1
 * @date 2023-05-13
 */
@RestController
@RequestMapping("/system/amsg")
public class AmsgController extends BaseController
{
    @Autowired
    private IAmsgService amsgService;

    /**
     * 查询amsg列表
     */
    @PreAuthorize("@ss.hasPermi('system:amsg:list')")
    @GetMapping("/list")
    public TableDataInfo list(Amsg amsg)
    {
        startPage();
        List<Amsg> list = amsgService.selectAmsgList(amsg);
        return getDataTable(list);
    }

    /**
     * 导出amsg列表
     */
    @PreAuthorize("@ss.hasPermi('system:amsg:export')")
    @Log(title = "amsg", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Amsg amsg)
    {
        List<Amsg> list = amsgService.selectAmsgList(amsg);
        ExcelUtil<Amsg> util = new ExcelUtil<Amsg>(Amsg.class);
        util.exportExcel(response, list, "amsg数据");
    }

    /**
     * 获取amsg详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:amsg:query')")
    @GetMapping(value = "/{way}")
    public AjaxResult getInfo(@PathVariable("way") Long way)
    {
        return success(amsgService.selectAmsgByWay(way));
    }

    /**
     * 新增amsg
     */
    @PreAuthorize("@ss.hasPermi('system:amsg:add')")
    @Log(title = "amsg", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Amsg amsg)
    {
        return toAjax(amsgService.insertAmsg(amsg));
    }

    /**
     * 修改amsg
     */
    @PreAuthorize("@ss.hasPermi('system:amsg:edit')")
    @Log(title = "amsg", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Amsg amsg)
    {
        return toAjax(amsgService.updateAmsg(amsg));
    }

    /**
     * 删除amsg
     */
    @PreAuthorize("@ss.hasPermi('system:amsg:remove')")
    @Log(title = "amsg", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ways}")
    public AjaxResult remove(@PathVariable Long[] ways)
    {
        return toAjax(amsgService.deleteAmsgByWays(ways));
    }
}
