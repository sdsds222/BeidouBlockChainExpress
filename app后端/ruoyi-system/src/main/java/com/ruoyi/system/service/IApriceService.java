package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Aprice;

/**
 * priceService接口
 * 
 * @author t1
 * @date 2023-05-13
 */
public interface IApriceService 
{
    /**
     * 查询price
     * 
     * @param way price主键
     * @return price
     */
    public Aprice selectApriceByWay(Long way);

    /**
     * 查询price列表
     * 
     * @param aprice price
     * @return price集合
     */
    public List<Aprice> selectApriceList(Aprice aprice);

    /**
     * 新增price
     * 
     * @param aprice price
     * @return 结果
     */
    public int insertAprice(Aprice aprice);

    /**
     * 修改price
     * 
     * @param aprice price
     * @return 结果
     */
    public int updateAprice(Aprice aprice);

    /**
     * 批量删除price
     * 
     * @param ways 需要删除的price主键集合
     * @return 结果
     */
    public int deleteApriceByWays(Long[] ways);

    /**
     * 删除price信息
     * 
     * @param way price主键
     * @return 结果
     */
    public int deleteApriceByWay(Long way);
}
