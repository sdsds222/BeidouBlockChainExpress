package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AuserCard;

/**
 * 总信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-30
 */
public interface AuserCardMapper 
{
    /**
     * 查询总信息
     * 
     * @param way 总信息主键
     * @return 总信息
     */
    public AuserCard selectAuserCardByWay(Long way);

    public AuserCard selectAuserCardByRfid(String rfid);
    /**
     * 查询总信息列表
     * 
     * @param auserCard 总信息
     * @return 总信息集合
     */
    public List<AuserCard> selectAuserCardList(AuserCard auserCard);

    /**
     * 新增总信息
     * 
     * @param auserCard 总信息
     * @return 结果
     */
    public int insertAuserCard(AuserCard auserCard);

    /**
     * 修改总信息
     * 
     * @param auserCard 总信息
     * @return 结果
     */
    public int updateAuserCard(AuserCard auserCard);

    /**
     * 删除总信息
     * 
     * @param way 总信息主键
     * @return 结果
     */
    public int deleteAuserCardByWay(Long way);

    /**
     * 批量删除总信息
     * 
     * @param ways 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAuserCardByWays(Long[] ways);
}
