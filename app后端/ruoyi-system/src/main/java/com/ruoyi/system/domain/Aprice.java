package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * price对象 aprice
 * 
 * @author t1
 * @date 2023-05-13
 */
public class Aprice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 价格 */
    @Excel(name = "价格")
    private Long price;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateTime;

    /** 条数 */
    private Long way;

    public void setPrice(Long price) 
    {
        this.price = price;
    }

    public Long getPrice() 
    {
        return price;
    }
    public void setDateTime(Date dateTime) 
    {
        this.dateTime = dateTime;
    }

    public Date getDateTime() 
    {
        return dateTime;
    }
    public void setWay(Long way) 
    {
        this.way = way;
    }

    public Long getWay() 
    {
        return way;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("price", getPrice())
            .append("dateTime", getDateTime())
            .append("way", getWay())
            .toString();
    }
}
