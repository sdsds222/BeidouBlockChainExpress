package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.AuserCard;
import com.ruoyi.system.service.IAuserCardService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 总信息Controller
 * 
 * @author ruoyi
 * @date 2023-04-30
 */
@RestController
@RequestMapping("/system/card")
public class AuserCardController extends BaseController
{
    @Autowired
    private IAuserCardService auserCardService;

    /**
     * 查询总信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:card:list')")
    @GetMapping("/list")
    public TableDataInfo list(AuserCard auserCard)
    {
        startPage();
        List<AuserCard> list = auserCardService.selectAuserCardList(auserCard);
        return getDataTable(list);
    }

    /**
     * 导出总信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:card:export')")
    @Log(title = "总信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AuserCard auserCard)
    {
        List<AuserCard> list = auserCardService.selectAuserCardList(auserCard);
        ExcelUtil<AuserCard> util = new ExcelUtil<AuserCard>(AuserCard.class);
        util.exportExcel(response, list, "总信息数据");
    }

    /**
     * 获取总信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:card:query')")
    @GetMapping(value = "/{way}")
    public AjaxResult getInfo(@PathVariable("way") Long way)
    {
        return success(auserCardService.selectAuserCardByWay(way));
    }

    /**
     * 新增总信息
     */
    @PreAuthorize("@ss.hasPermi('system:card:add')")
    @Log(title = "总信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AuserCard auserCard)
    {
        return toAjax(auserCardService.insertAuserCard(auserCard));
    }

    /**
     * 修改总信息
     */
    @PreAuthorize("@ss.hasPermi('system:card:edit')")
    @Log(title = "总信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AuserCard auserCard)
    {
        return toAjax(auserCardService.updateAuserCard(auserCard));
    }

    /**
     * 删除总信息
     */
    @PreAuthorize("@ss.hasPermi('system:card:remove')")
    @Log(title = "总信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ways}")
    public AjaxResult remove(@PathVariable Long[] ways)
    {
        return toAjax(auserCardService.deleteAuserCardByWays(ways));
    }
}
