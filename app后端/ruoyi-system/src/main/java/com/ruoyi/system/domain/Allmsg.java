package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * allmsg1对象 allmsg
 * 
 * @author ruoyi
 * @date 2023-05-16
 */
public class Allmsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 水 */
    @Excel(name = "水")
    private String water;

    /** 烟 */
    @Excel(name = "烟")
    private String gas;

    /** 人 */
    @Excel(name = "人")
    private String person;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateTime;

    /** 条数 */
    private Long way;

    public void setWater(String water) 
    {
        this.water = water;
    }

    public String getWater() 
    {
        return water;
    }
    public void setGas(String gas) 
    {
        this.gas = gas;
    }

    public String getGas() 
    {
        return gas;
    }
    public void setPerson(String person) 
    {
        this.person = person;
    }

    public String getPerson() 
    {
        return person;
    }
    public void setDateTime(Date dateTime) 
    {
        this.dateTime = dateTime;
    }

    public Date getDateTime() 
    {
        return dateTime;
    }
    public void setWay(Long way) 
    {
        this.way = way;
    }

    public Long getWay() 
    {
        return way;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("water", getWater())
            .append("gas", getGas())
            .append("person", getPerson())
            .append("dateTime", getDateTime())
            .append("way", getWay())
            .toString();
    }
}
