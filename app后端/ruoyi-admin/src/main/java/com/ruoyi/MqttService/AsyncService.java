package com.ruoyi.MqttService;

import com.ruoyi.client.EmqClient;
import com.ruoyi.system.domain.enums.QosEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class AsyncService {

    @Resource
    private EmqClient emqClient;
    @Resource
    private EmilService emilService;
    private static String  nowdate="yyyyMMddHHmmss";


    //异步多线程调用
    @Async("asyncServiceExecutor")
    public void executeAsync() {
        log.info("关闭");
        try{
            emqClient.publish("LXL/callback", "door:0", QosEnum.Qos0, false);

            log.info("关闭消息已发送");
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        log.info("结束线程");
    }
    //异步多线程调用
    @Async("asyncServiceExecutor")
    public void executeAsync1() {
        log.info("开启");
        try{
            emqClient.publish("LXL/callback", "door:1", QosEnum.Qos0, false);

            log.info("开启消息已发送");
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
        log.info("结束线程");
    }

}
