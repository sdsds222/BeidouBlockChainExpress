package com.ruoyi.MqttService;


import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.util.Page;
import com.ruoyi.system.mapper.MqttMapper;
import com.ruoyi.system.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;
@Slf4j
@Service
public class MqttService {
    @Autowired
    MqttMapper mqttMapper;
@Autowired
private IAmsgService iAmsgService;
    @Autowired
    private IArfidService iArfidService;
    @Autowired
    AsyncService asyncService;
    @Autowired
    IApriceService iApriceService;

    @Autowired
    private ISanjimsgService iSanjimsgService;
    StopWatch stopWatch=new StopWatch();




    public void interData(Map map) {
        Date now = new Date();
        if(iArfidService.selectAllByRfid(map.get("rfid").toString())!=null){
            Arfid arfid=new Arfid();
            arfid.setRfid(map.get("rfid").toString());
            arfid.setStatus("0");
            arfid.setDateTime(now);
            iArfidService.insertArfid(arfid);
            asyncService.executeAsync();//下发CLOSE指令
            close();
        }
    }


    public void interData1(Map map) {

        Date now = new Date();
        if(iArfidService.selectAllByRfid(map.get("rfid").toString())!=null){
            Arfid arfid=new Arfid();
            arfid.setRfid(map.get("rfid").toString());
            arfid.setStatus("1");
            arfid.setDateTime(now);
            iArfidService.insertArfid(arfid);
            asyncService.executeAsync1();//下发OPEN指令
            open();
        }



    }
    public void interData2(Map map) {

            Date now = new Date();
        Sanjimsg sanjimsg =new Sanjimsg();
        sanjimsg.setHr(map.get("HR").toString());
        sanjimsg.setSpo2(map.get("SpO2").toString());
        sanjimsg.setStatus(map.get("FALLSTATUS").toString());
        sanjimsg.setLat(map.get("Latitude").toString());
        sanjimsg.setLng(map.get("Longitude").toString());
        sanjimsg.setDateTime(now);
        iSanjimsgService.insertSanjimsg(sanjimsg);


    }

    public void open(){
        Date now = new Date();
        stopWatch.start();
        Aprice aprice=new Aprice();
        aprice.setPrice(0L);
        aprice.setDateTime(now);
        iApriceService.insertAprice(aprice);

    }
    public long close(){
        Date now = new Date();
        stopWatch.stop();
        long i= (int) (stopWatch.getLastTaskTimeMillis() / 1000.0);
        Aprice aprice=new Aprice();
        aprice.setPrice(i);
        aprice.setDateTime(now);
        iApriceService.insertAprice(aprice);
       return i;

    }

    public List<Sanjimsg> getLastMsg(Page<Sanjimsg> page, HttpServletRequest request) {

        List<Sanjimsg> msg = mqttMapper.getLastMsg();
        return msg;
    }
    public List<Sanjimsg> getAllMsg(Page<Sanjimsg> page, HttpServletRequest request) {

        List<Sanjimsg> msg = mqttMapper.getAllMsg();
        return msg;
    }
    public List<AuserCard> getLastMsgById(String userId) {

        List<AuserCard> msg = mqttMapper.getLastMsgById(userId);
        return msg;
    }


}