package com.ruoyi.client;


import com.ruoyi.system.domain.FileProperties;
import com.ruoyi.system.domain.PhotoPathProperties;
import com.ruoyi.system.mapper.MqttMapper;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ImageMessage {

    private static String  nowdate="yyyyMMddHHmmss";
    @Autowired
    private FileProperties fileProperties;

    @Resource
    private MqttMapper mqttMapper;

    public  boolean toImage(MqttMessage message)  throws IOException {
        // 图片存储路径
        String path = fileProperties.getFileurl();

        // 判断是否有路径
        if (!new File(path).exists()) {
            new File(path).mkdirs();
        }

        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(nowdate);//可以方便地修改日期格式
        String Strdate = dateFormat.format(now);

        String fileName =Strdate+fileProperties.getFilename()+".png";

        PhotoPathProperties photoPathProperties = new PhotoPathProperties();
        photoPathProperties.setUrl(fileName);
        photoPathProperties.setDateTime(Strdate);
        mqttMapper.insertPath(photoPathProperties);

        File tempFile = new File(path,fileName);
        if (!tempFile.exists()) {
            tempFile.createNewFile();
        }

        String base64str= new String(message.getPayload());
            if (message.getPayload() == null) //图像数据为空
                return false;
            BASE64Decoder decoder = new BASE64Decoder();
            try
            {
                //Base64解码
                byte[] b = decoder.decodeBuffer(base64str);
                //  System.out.println("解码完成");
                for(int i=0;i<b.length;++i)
                {
                    if(b[i]<0)
                    {//调整异常数据
                        b[i]+=256;
                    }
                }
                 System.out.println("开始生成图片");
                //生成jpeg图片
                OutputStream out = new FileOutputStream(tempFile);
                out.write(b);
                out.flush();
                out.close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
    }
}
