package com.ruoyi.client;

import com.alibaba.fastjson.JSON;
import com.ruoyi.MqttService.MqttService;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class MyMqttMessage {

    @Autowired
    private MqttService mqttService;
    private static final Logger log= LoggerFactory.getLogger(MqttCallback.class);

    private static String  nowdate="yyyyMMddHHmmss";

    public  boolean Close(org.eclipse.paho.client.mqttv3.MqttMessage message) {
        log.info("RFID");

        String msg= new String(message.getPayload());
        Map maps = (Map) JSON.parse(msg);

        mqttService.interData(maps);
        return true;
    }
    public  boolean Open(org.eclipse.paho.client.mqttv3.MqttMessage message) {
        log.info("RFID");

        String msg= new String(message.getPayload());
        Map maps = (Map) JSON.parse(msg);

        mqttService.interData1(maps);
        return true;
    }

    public  boolean Msg(org.eclipse.paho.client.mqttv3.MqttMessage message) {
        log.info("信息存储至数据库");

        String msg= new String(message.getPayload());
        Map maps = (Map) JSON.parse(msg);

        mqttService.interData2(maps);
        return true;
    }
    public  boolean Local(org.eclipse.paho.client.mqttv3.MqttMessage message) {
        log.info("信息存储至数据库");

        String msg= new String(message.getPayload());
        Map maps = (Map) JSON.parse(msg);

        mqttService.interData2(maps);
        return true;
    }
    public  boolean Status(org.eclipse.paho.client.mqttv3.MqttMessage message) {
        log.info("信息存储至数据库");

        String msg= new String(message.getPayload());
        Map maps = (Map) JSON.parse(msg);

        mqttService.interData2(maps);
        return true;
    }
}
