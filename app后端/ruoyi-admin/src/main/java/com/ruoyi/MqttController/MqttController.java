package com.ruoyi.MqttController;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.MqttService.EmilService;
import com.ruoyi.MqttService.MqttService;
import com.ruoyi.client.EmqClient;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.MqttMapper;
import com.ruoyi.system.domain.enums.QosEnum;
import com.ruoyi.system.domain.util.*;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/Mqtt")
public class MqttController {

static boolean i=false;
    @Autowired
    private EmqClient emqClient;

    @Resource
    private MqttMapper mqttMapper;

    @Autowired
    private MqttService mqttService;

    @Autowired
    private EmilService emilService;

    private static final Logger log= LoggerFactory.getLogger(MqttController.class);

    private static final String  nowdate="yyyyMMddHHmmss";

    @RequestMapping("/toSelectLastMsg")
    @ResponseBody
    public List<Sanjimsg> toSelectLastMsg(Page<Sanjimsg> page , HttpServletRequest request){
        List<Sanjimsg> msg = mqttService.getLastMsg(page,request);
        return msg;
    }

    @RequestMapping("/ListAll")
    @ResponseBody
    public List<Sanjimsg> ListAll(Page<Sanjimsg> page , HttpServletRequest request){
        List<Sanjimsg> msg = mqttService.getAllMsg(page,request);
        return msg;
    }
    @RequestMapping("/toSelectLastRFID")
    @ResponseBody
    public List<Arfid> toSelectLastRFID(Page<Amsg> page , HttpServletRequest request){
        List<Arfid> msg = mqttMapper.getLastrfid();
        return msg;
    }
    @RequestMapping("/toSelectLastPRICE")
    @ResponseBody
    public List<Aprice> toSelectLastPRICE(Page<Aprice> page , HttpServletRequest request){
        List<Aprice> msg = mqttMapper.getLastprice();
        return msg;
    }

//    @RequestMapping("/toSelectLastMsgById")
//    @ResponseBody
//    public List<Amsg> toSelectLastMsgById(Page<Amsg> page , HttpServletRequest request){
//        List<Amsg> msg = mqttService.getLastMsg(page,request);
//        return msg;
//    }

    @GetMapping(value = "/{userId}")
    public List<AuserCard> getInfoByUser(@PathVariable("userId") String userId)
    {
        List<AuserCard> msg = mqttService.getLastMsgById(userId);
        return msg;
    }


    @RequestMapping("/toSendEml")
    @ResponseBody
    public void toSendEml(Page page, HttpServletRequest request){
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(nowdate);//可以方便地修改日期格式
        String Str = dateFormat.format(now);
        String to = "aaaa443301@outlook.com";
        emilService.send(to, "消息提醒测试eeeee",Str);
    }



    /**
     * @param
     * @paramm param
     * @功能描述  传图片
     */
    @RequestMapping("/get-face-picture")
    public String  getUserFacePicture(HttpServletRequest request, HttpServletResponse response) {

        String sessionId = request.getHeader("token");


        byte[] faceImageByte = new byte[0];
        try {
            faceImageByte = image2byte2();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //转成base64格式
        String base64EncoderImg = Base64.encodeBase64String(faceImageByte);

        return "data:image/"+ "png" +";base64,"+base64EncoderImg;

    }

    StopWatch stopWatch=new StopWatch();


//    @RequestMapping("/open")
//    @ResponseBody
//    public String open(HttpSession session){
//      mqttService.open();
//        return "已开启电车";
//    }
//    @RequestMapping("/close")
//    @ResponseBody
//    public String close(HttpSession session){
//        long i=mqttService.close();
//        return "已关闭电车"+i+"元";
//    }



    @RequestMapping("/open")
    @ResponseBody
    public boolean getBeep(HttpSession session){

            emqClient.publish("JYC/callback", "door:1", QosEnum.Qos0, false);
            log.info("开");
            i=true;



        return true;
    }

    //图片到byte数组
    public byte[] image2byte2(){
        byte[] data = null;
        FileImageInputStream input = null;
        try {
            PhotoPathProperties photoPathProperties= mqttMapper.selectLastPath();
            input = new FileImageInputStream(new File("D:\\IMAGE\\"+photoPathProperties.getUrl()));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int numBytesRead = 0;
            while ((numBytesRead = input.read(buf)) != -1) {
                output.write(buf, 0, numBytesRead);
            }
            data = output.toByteArray();
            output.close();
            input.close();
        }
        catch (FileNotFoundException ex1) {
            ex1.printStackTrace();
        }
        catch (IOException ex1) {
            ex1.printStackTrace();
        }
        return data;
    }

}
