import request from '@/utils/request'

// 查询图片
export function getPictr() {
  return request({
    url: '/Mqtt/get-face-picture',
    method: 'get',
  })
}

export function sendMqtt() {
  return request({
    url: '/Mqtt/getBeep',
    method: 'get',
  })
}

export function ListMsg() {
  return request({
    url: '/Mqtt/toSelectLastMsg',
    method: 'get',
    
  })
}

export function ListMsgById(userId) {
  return request({
    url: '/Mqtt/'+ userId,
    method: 'get',
    
  })
}

export function getQRcode() {
  return request({
    url: '/qrcode/getQRCode2',
    method: 'get',

  })
}