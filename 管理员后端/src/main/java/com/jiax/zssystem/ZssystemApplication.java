package com.jiax.zssystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZssystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZssystemApplication.class, args);
    }

}
