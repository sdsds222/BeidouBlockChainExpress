package com.jiax.zssystem.service;

import com.jiax.zssystem.mapper.userMapper;
import com.jiax.zssystem.pojo.UserProperties;
import com.jiax.zssystem.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class MsgService {
    private static final String  nowdate="yyyyMMddHHmmss";
    @Autowired
    private userMapper userMapper;

    public void interData(Map map) {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(nowdate);
        String Str = dateFormat.format(now);
        UserProperties userProperties= UserProperties.builder()
                .Lat(map.get("Longitude").toString())
                .Lng(map.get("Latitude").toString())
                .local("1")
                .dateTime(Str)
                .build();
        userMapper.insertUser(userProperties);


    }

    public List<UserProperties> getAllUser(Page<UserProperties> page, HttpServletRequest request) {


        List<UserProperties> allBms = userMapper.getAllUser();
        return allBms;
    }

    public Map SelectUser(Map map){
        Map maps=userMapper.getLastUser();
        return maps;
    }


    public void updateUser(String he) {
       userMapper.updateUser(he);


    }

}
