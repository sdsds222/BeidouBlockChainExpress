package com.jiax.zssystem.client;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

@Component
public class MessageCallback implements MqttCallback {
    @Resource
    private EmqClient emqClient;

    private Timer timer;

    @Resource
    private GetMessage getMessage;

    private static final Logger log= LoggerFactory.getLogger(MqttCallback.class);
    public static Map<String, Object> map = new HashMap<>();



//    丢失对服务端之后进行的回调
    @Override
    public void connectionLost(Throwable throwable) {
//    资源的清理，重连，可以运用Emqclient 里面的重连方法
        emqClient.reConnect();
        log.error("丢失对服务端连接");
    }

//    应用收到信息后触发的回调
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {


        String msg=new String(message.getPayload());
        String theMsg = MessageFormat.format("id:{0} is arrived for topic {1}.", msg);
        System.out.println(theMsg);
        log.info("订阅者订阅到了消息,topic={},messageid={},qos={}",
                topic,
                message.getId(),
                message.getQos(),
                message.getPayload()

        );

        if(topic.equals("BLOCK")) {
            log.info("信息存储至数据库");
            getMessage.toMsg(message);

        }




    }


//    发布者发布完成之后产生的回调
    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        int messageId = token.getMessageId();
        String[] topics = token.getTopics();
        log.info("消息发布完成，messageid={},topic={},msg={}",messageId,topics);

    }






}
