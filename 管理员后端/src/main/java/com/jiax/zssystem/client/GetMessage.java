package com.jiax.zssystem.client;

import com.jiax.zssystem.service.MsgService;
import com.alibaba.fastjson.JSON;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;

@Component
public class GetMessage {
    @Resource
    private MsgService msgService;

    private static final Logger log= LoggerFactory.getLogger(MqttCallback.class);


    public  boolean toMsg(MqttMessage message)  throws IOException {
        String msg= new String(message.getPayload());
        Map maps = (Map) JSON.parse(msg);
        msgService.interData(maps);
        return true;
    }

}
