package com.jiax.zssystem.util;

import java.math.BigInteger;

public interface Constant {

    public static final Integer CONNECT_SECONDS = 30;
    public static final Integer CONNECT_SLEEP_PER_MILLIS = 1;
    public static final Integer TIME_OUT = 30000;

    BigInteger gasPrice = new BigInteger("300000000");
    BigInteger gasLimit = new BigInteger("300000000");

    String accountContractAddress = "0x4b1d00bae7ae9377782d6d8d4c599bdf258a4882";
    String tradeContractAddress = "0x8f6884097d8a4b5598f68139118ff4337db6cc15";

    String projectPath = "D:\\A\\ZS\\ZS\\src/main/resources/static/";
    String projectBuildPath = "D:\\A\\ZS\\ZS/target/classes/static/";

}
