package com.jiax.zssystem.mapper;

import com.jiax.zssystem.pojo.UserProperties;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface userMapper {
    int insertUser(UserProperties userProperties);
    List<UserProperties> getAllUser();

    Map getLastUser();
    List<UserProperties> getLastUser2();
    void updateUser(String he);

}
