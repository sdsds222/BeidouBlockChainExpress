package com.jiax.zssystem.controller;
import com.jiax.zssystem.pojo.UserProperties;
import com.jiax.zssystem.service.MsgService;
import com.jiax.zssystem.util.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;




@Controller
@RequestMapping("/Block")
public class UserController {
    @Resource
    private MsgService msgService;
    @Resource
    private com.jiax.zssystem.mapper.userMapper userMapper;
    @RequestMapping("/toSelectmsg")
    @ResponseBody
    public List<UserProperties> toSelectBms(Page<UserProperties> page , HttpServletRequest request){
        List<UserProperties> allBms = msgService.getAllUser(page,request);
        return allBms;
    }
    @RequestMapping("/toSetHe")
    @ResponseBody
    public String toSetSpeed(@RequestParam("he") String he,
                             Model model, HttpSession session)   {
           msgService.updateUser(he);
           return  he;
    }

    @RequestMapping("/toSelectLocal")
    @ResponseBody
    public List<UserProperties> toSelectLocal(Page<UserProperties> page , HttpServletRequest request){
        List<UserProperties> allBms = userMapper.getLastUser2();
        return allBms;
    }






}