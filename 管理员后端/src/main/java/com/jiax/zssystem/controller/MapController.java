package com.jiax.zssystem.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class MapController {

    @RequestMapping("/toMap")
    public String toMap(HttpSession session){
        return "map";
    }

    @RequestMapping("/toMap2")
    public String toMap2(HttpSession session){
        return "map2";
    }

    @RequestMapping("/toMap3")
    public String toMap3(HttpSession session){
        return "map3";
    }


    @RequestMapping("/toIndex")
    public String toIndex(HttpSession session){
        return "market";
    }


}
