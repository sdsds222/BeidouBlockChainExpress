package com.jiax.zssystem.controller;

import com.jiax.zssystem.mapper.userMapper;
import com.jiax.zssystem.pojo.UserProperties;
import com.jiax.zssystem.util.QRCodeUtil;
import com.jiax.zssystem.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * QRCodeController
 * </p>
 *
 * @author rcbb.cc
 * @date 2022/8/28
 */
@RestController
@RequestMapping("/qrcode")
public class QRCodeController {

    @Autowired
    private userMapper userMapper;

    /**
     * 根据 content 生成二维码
     * @param //content
     * @param width
     * @param height
     * @return
     */
    @GetMapping("/getQRCodeBase64")
    public R getQRCode(//@RequestParam("content") String content,
                       @RequestParam(value = "logoUrl", required = false) String logoUrl,
                       @RequestParam(value = "width", required = false) Integer width,
                       @RequestParam(value = "height", required = false) Integer height) {
        List<UserProperties> allBms = userMapper.getAllUser();

        String content1= String.valueOf(allBms);
        return R.ok(QRCodeUtil.getBase64QRCode(content1, logoUrl));
    }

    /**
     * 根据 content 生成二维码
     */
    @GetMapping(value = "/getQRCode")
    @ResponseBody
    public void getQRCode(HttpServletResponse response
//                          @RequestParam("content") String content,
//                          @RequestParam(value = "logoUrl", required = false) String logoUrl
    ) throws Exception {
                  Map map=userMapper.getLastUser();
       //List<UserProperties> allBms = userMapper.getAllUser();
        System.out.println(map);
        String content1= String.valueOf(map);
        ServletOutputStream stream = null;
        try {
            String i=map.get("local").toString();
            System.out.println(i);
            if(i.equals("0")){
                stream = response.getOutputStream();
                QRCodeUtil.getQRCodeRed(content1, stream);
            }
           else {
                stream = response.getOutputStream();
                QRCodeUtil.getQRCodeGreen(content1, stream);
            }
        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            if (stream != null) {
                stream.flush();
                stream.close();
            }
        }
    }


    /**
     * @param
     * @paramm param
     * @功能描述  传图片
     */
    @GetMapping("/GenerateCode")
    public void GenerateCode(HttpServletRequest request, HttpServletResponse response) {

        //读取文件   在用BufferedImage 读取
        File file = new File("E:\\IMAGE\\1.png");
        BufferedImage bu = null;
        try {
            bu = ImageIO.read(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //传到浏览器上
        try {
            ImageIO.write( bu, "PNG", response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
