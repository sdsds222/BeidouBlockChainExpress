package com.jiax.zssystem.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserProperties {
    private String Lng;
    private String Lat;
    private String local;
    private String dateTime;
}
